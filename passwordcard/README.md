# README #

A two sides CR-80 size Password Card

* Version 1

### How do I get set up? ###

* Clone
* Open in your browser

### Use ###

* Pick a direction. You don't have to go from left to right to read your passwords, you can go from right to left, up or down, or even diagonally. It's probably a good idea to pick one direction though, even if you use your PasswordCard for multiple passwords.
* Pick a password length. Eight is pretty secure and usually acceptable. Again, it's a good idea to pick one length.
* Pick a color and a symbol for each password. You can use one password for all your sites, but that still wouldn't be very safe. It's a good idea to at least have different passwords for very important sites, such as Internet banking sites.
* Use the top alphabet and a color or colors to generate a unique key for extra layer of protection
* Combine sides or use only digits/characters
 
### Contribution guidelines ###

Idea based on www.passwordcard.org